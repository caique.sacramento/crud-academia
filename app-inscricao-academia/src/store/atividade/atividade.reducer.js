import { TYPES } from './atividade.action'

const INITIAL_STATE = {
    all: [],
    loading: false,
    details: {}
};

const reducer = (state = INITIAL_STATE, action) => { // tamara recebe
    switch (action.type) {
        case TYPES.ATIVIDADE_LOADING:
            state.loading = action.status
            return state;
        case TYPES.ATIVIDADE_ALL:
            state.all = action.data
            state.loading = false
            return state;
        case TYPES.ATIVIDADE_DETAILS:
            state.details = action.data
            state.loading = false
            return state;
        default:
            return state;
    }
};

export default reducer;
