
import {
    createServiceAtividade,
    deleteServiceSubscription,
    getServiceAllAtividades,
    getServiceDetalhe,
    removeServiceAtividade,
    updateServiceAtividade
} from "../../services/atividade.service"

export const TYPES = {
    ATIVIDADE_LOADING: "ATIVIDADE_LOADING",
    ATIVIDADE_ALL: "ATIVIDADE_ALL",
    ATIVIDADE_DETAILS: "ATIVIDADE_DETAILS",
}

export const getatividadeAll = () => {
    return async (dispatch) => {
        // carregar o loading antes de chamar o serviço
        dispatch({ type: TYPES.ATIVIDADE_LOADING, status: true })

        try {
            const all = await getServiceAllAtividades()
            dispatch({
                type: TYPES.ATIVIDADE_ALL,
                data: all.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ATIVIDADE_LOADING, status: false })
            console.log('aconteceu um ERRO": disparar um e-mail para Admin')
        }
    }
}

export const createAtividade = (atividade) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ATIVIDADE_LOADING, status: true })
        try {
            await createServiceAtividade(atividade)
            dispatch(getatividadeAll())

        } catch (error) {
            dispatch({ type: TYPES.ATIVIDADE_LOADING, status: false })
            console.log('aconteceu um ERRO": Erro ao criar usuario')
        }

    }
}
export const updateAtividade = ({ id, professor, name, start_date }) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ATIVIDADE_LOADING, status: true })
        try {
            const data = { professor, name, start_date }

            await updateServiceAtividade(id, data)
            dispatch(getatividadeAll())

        } catch (error) {
            dispatch({ type: TYPES.ATIVIDADE_LOADING, status: false })
            console.log('aconteceu um ERRO": Erro ao criar usuario')
        }

    }
}
export const removeAtividade = (id_atividade) => {
    return async (dispatch) => {
        dispatch({ type: TYPES.ATIVIDADE_LOADING, status: true })
        try {
            // debugger
            await removeServiceAtividade(id_atividade)
            dispatch(getatividadeAll())

        } catch (error) {
            dispatch({ type: TYPES.ATIVIDADE_LOADING, status: false })
            console.log('aconteceu um ERRO": Erro ao Excluir atividade::', error)
        }

    }
}
export const getDetails = (id_atividade) => {
    return async (dispatch) => {
        try {

            const res = await getServiceDetalhe(id_atividade);
            res.data.registered = res.data.inscricoes.length > 0;
            dispatch({
                type: TYPES.ATIVIDADE_DETAILS,
                data: res.data
            })
        } catch (error) {
            dispatch({ type: TYPES.ATIVIDADE_LOADING, status: false })
            console.log('aconteceu um ERRO": Erro ao detalhar usuario')
        }

    }
}
export const deleteSubscription = (id_aluno) => {
    return async (dispatch, getState) => {
        const { atividade } = getState()
        dispatch({ type: TYPES.ATIVIDADE_LOADING, status: true })

        try {
            const res = await deleteServiceSubscription(atividade.details.id, id_aluno)

            if (res.status === 200) {
                dispatch(getDetails(atividade.details.id))
            }


        } catch (error) {
            dispatch({ type: TYPES.ATIVIDADE_LOADING, status: false })
            console.log('aconteceu um ERRO": Erro ao criar usuario')

        }

    }

}




