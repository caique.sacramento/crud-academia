//import as libs
import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from 'redux-thunk'

// importação dos reducers
import SignReducer from "./auth/auth.reducer"
import Atividadereducer from "./atividade/atividade.reducer"
import AlunoReducer from "./aluno/aluno.reducer"

const reducers = combineReducers({
    auth: SignReducer,
    atividade: Atividadereducer,
    aluno: AlunoReducer,
})

// middlewares de redux
const middlewares = [thunk]

// compose junta os middlewares e ferramentas de debug

const compose = composeWithDevTools(applyMiddleware(...middlewares))

// criar a store do redux 

const store = createStore(reducers, compose)


export default store;