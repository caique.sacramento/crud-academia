
import {
    createSubStudentInAtividade,
    getServiceStudentAll,
    putServiceStudent,
    getServiceStudent,
    removeSubStudentInAtividade
} from "../../services/aluno.service"
import { getDetails } from "../atividade/atividade.action"

export const TYPES = {
    STUDEND_ALL: "STUDEND_ALL",
    STUDENT_PROFILE: "STUDENT_PROFILE"
}

export const getStudentAll = () => {
    return async (dispatch) => {

        try {
            const all = await getServiceStudentAll()
            dispatch({
                type: TYPES.STUDEND_ALL,
                data: all.data
            })
        } catch (error) {
            console.log('aconteceu um ERRO": disparar um e-mail para Admin')
        }
    }
}

export const updateProfile = ({ id, ...data }) => {
    return async (dispatch) => {
        try {

            const response = await putServiceStudent(id, data)
            if (response) {
                const aluno = await getServiceStudent(id)

                dispatch({
                    type: TYPES.STUDENT_PROFILE,
                    data: aluno.data
                })
            }


        } catch (error) {
            console.log('aconteceu um ERRO": disparar um e-mail para Admin')
        }
    }
}


export const removeSubscription = (id_atividade, id_inscricao) => {
    return async (dispatch) => {

        try {
            const all = await removeSubStudentInAtividade(id_atividade, id_inscricao)
            if (all.data) {
                dispatch(getDetails(id_atividade))
            }
        } catch (error) {
            console.log('aconteceu um ERRO": disparar um e-mail para Admin')
        }
    }
}

export const createSubscription = (id) => {
    return async (dispatch) => {

        try {
            const all = await createSubStudentInAtividade(id)
            if (all.data) {
                dispatch(getDetails(id))
            }
        } catch (error) {
            console.log('aconteceu um ERRO": disparar um e-mail para Admin')
        }

    }
}
