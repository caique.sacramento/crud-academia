import { Col, Label, Row, FormGroup, Input } from 'reactstrap';
import styled from 'styled-components';

const FormAtividade = ({ state }) => {
    const [form, setForm] = state

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }
    return (
        <BoxInscricao>
            <Col xs="12" sm="12" md="12" lg="12">
                <FormGroup>
                    <Label for="name">Nome do Atividade</Label>
                    <Input type="text" id="name" value={form.name || ""} onChange={handleChange}
                        name="name" placeholder="Insira seu nome" />
                </FormGroup>
                <FormGroup>
                    <Label for="name">Nome do Coordenador</Label>
                    <Input type="text" id="professor" value={form.professor || ""} onChange={handleChange}
                        name="professor" placeholder="Insira do professor" />
                </FormGroup>
                <FormGroup>
                    <Label for="name">Data de Início</Label>
                    <Input type="date" id="start_date" value={form.start_date || ""} onChange={handleChange}
                        name="start_date" />
                </FormGroup>

            </Col>
        </BoxInscricao>
    )

}

export default FormAtividade;


const BoxInscricao = styled(Row)``
