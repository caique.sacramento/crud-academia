import { Table } from 'reactstrap'
import { BiTrash, BiEdit } from 'react-icons/bi'
import { Link } from 'react-router-dom'


const TableList = (props) => {

    const { atividades, editarAtividade, excluirAtividade } = props

    return (
        <Table>
            <thead>
                <tr>
                    <th>NOME DO ATIVIDADE</th>
                    <th>QUANT.</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {atividades?.map((atividade, i) => (
                    <tr key={i}>
                        <td><Link to={`detalhes/${atividade.id}`}>{atividade.name}</Link></td>
                        <td>{atividade.qtd_inscricoes}</td>
                        <td>
                            <BiEdit style={{ cursor: "pointer" }} className="text-info mr-1 font-weight-normal" onClick={() => editarAtividade(atividade.id)} />
                            <BiTrash style={{ cursor: "pointer" }} className="text-danger font-weight-normal" onClick={() => excluirAtividade(atividade)} />
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    )

}

export default TableList;