import { Link } from 'react-router-dom';
import {
    Button,
    Card, CardBody,
    CardTitle, CardSubtitle
} from 'reactstrap';
import styled from 'styled-components';

const CardItem = (props) => {
    const { name, id, status } = props.item;

    const EhAtividade = ({ isFalse }) => {
        if (isFalse) {
            return <CardSubtitle tag="h6" className="mb-2 text-muted">Atividade</CardSubtitle>
        } else {
            return <small>Palestra</small>
        }
    }

    return (
        <SCard>
            <CardBody>
                <CardTitle tag="h5">{name}</CardTitle>
                <EhAtividade isFalse={status}  />
                <Button size="sm"  tag={Link} to={`/detalhes/${id}`} color="link">Saiba mais...</Button>
            </CardBody>
        </SCard>
    )
}

export default CardItem;


const SCard =  styled(Card)`

    background-color: #fafafa;

    :hover {
        background-color: #ddd;
        transition:1s;
        opacity: 0.9;
    }

`