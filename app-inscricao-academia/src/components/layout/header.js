import React, { useState } from 'react';
import { NavLink as RRDNavLink } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    NavItem,
    NavLink,  
    Container,
    Tooltip,
    Nav, UncontrolledDropdown, DropdownItem, DropdownToggle, DropdownMenu

} from 'reactstrap';
import styled, { css } from 'styled-components';
import { BiDumbbell } from 'react-icons/bi'
import { useSelector, useDispatch } from 'react-redux';
import { logoutAction } from '../../store/auth/auth.action';
import { isAuthenticated } from '../../config/auth';
import history from '../../config/history';

const Header = (props) => {
    const dispatch = useDispatch()
    const [isOpen, setIsOpen] = useState(false);
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);
    const toggle = () => setIsOpen(!isOpen);
    const usuario = useSelector(state => state.auth.usuario)
    const isAdmin = useSelector(state => state.auth.isAdmin)

    const logout = () => {
        dispatch(logoutAction())
    }



    return (
        <header>
            <SNavbar color="dark" dark expand="md">
                <Container>

                    <NavbarBrand tag={RRDNavLink} to="/" id="logoMain"> <IconLogo /> CRUD Academia </NavbarBrand>
                    <Tooltip placement="top" isOpen={tooltipOpen} autohide={false} target="logoMain" toggle={toggleTooltip}>
                        Voltar ao Menu Principal
                    </Tooltip>
                    {isAuthenticated() ? (
                        <React.Fragment>
                            <SCollapse isOpen={isOpen} navbar>
                                <Nav className="mr-auto" navbar>
                                    <NavItem>
                                        <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/">Inicio</SNavLink>
                                    </NavItem>
                                    {isAdmin ? (
                                        <>
                                            <NavItem>
                                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/atividades" >Atividades</SNavLink>
                                            </NavItem>
                                            <NavItem>
                                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/alunos" >Alunos</SNavLink>
                                            </NavItem>
                                        </>
                                    ) : ""}
                                
                                </Nav>
                            </SCollapse>

                            <Nav >
                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        {usuario.nome}
                                    </DropdownToggle>
                                    <DropdownMenu>

                                        <DropdownItem onClick={() => history.push('/perfil')}>Perfil</DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem onClick={logout}>Sair</DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </Nav>
                        </React.Fragment>
                    ) : ""}
                    {isAdmin ? (
                        <NavbarToggler onClick={toggle} />
                    ) : ""}
                </Container>
            </SNavbar>
        </header>
    )
}

export default Header


const SNavbar = styled(Navbar)`
   ${({ theme }) => css`
    background-color: ${theme.primary} !important;
    border-bottom: 5px solid ${theme.secondary};

    a {
        color: #fff !important;
    }
   `}
`

const SNavLink = styled(NavLink)`
    margin: auto 5px;
    border-radius: 5px;

    &.active {
        color: #fff !important;
        background-color: #7B3E19 !important;
    }

    @media (max-width: 767.98px) {
        margin:6px 0;
        
    }

`
const SCollapse = styled(Collapse)`
    width100%
`

const IconLogo = styled(BiDumbbell)`
    font-size: 26px;
    margin-top: -4px
`