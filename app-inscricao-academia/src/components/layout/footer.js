import styled, { css } from "styled-components";

const Footer = () => (
    <SFooter>
        <p>Projeto do Instituto Infnet (www.infnet.edu.br) || Feito por Caique Murilo Sacramento</p>
    </SFooter>
);

export default Footer;


const SFooter = styled.footer`
    ${({ theme }) => css`
        background-color: ${theme.primary} !important;
        color: #fff;
        border-top: 2px solid #4b8EC7;
        text-align:center;
        padding: 10px;
    `}
`