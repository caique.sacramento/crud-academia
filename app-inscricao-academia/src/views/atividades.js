import React, { useEffect, useState } from 'react';
import { TitlePage } from "../assets/styled";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { getatividadeAll, removeAtividade, createAtividade, getDetails, updateAtividade } from '../store/atividade/atividade.action';
import TableList from '../components/atividades/tableList';
import FormAtividade from '../components/atividades/form';
import ReactSwal from '../plugins/swal';
import moment from 'moment';

const GerenciarAtividades = () => {
    document.title = 'CRUD Academia - atividades'
    // state
    const dispatch = useDispatch()
    const [modal, setModal] = useState(false)
    const [isUpdate, setUpdate] = useState(false)
    const stateForm = useState({})
    const [form, setForm] = stateForm

    // store
    const atividades = useSelector(state => state.atividade.all)
    const detalhe = useSelector(state => state.atividade.details)

    // functions
    const toggle = (hadEvent) => {
        if (hadEvent) {
            setUpdate(false)
            setForm({})
        }
        setModal(!modal)
    };

    useEffect(() => {
        dispatch(getatividadeAll());
    }, [dispatch])


    const editTable = (id) => {
        dispatch(getDetails(id))
            .then(() => {
                setForm({ ...form, id })
                setUpdate(true)
                toggle()

            })
    }

    const deleteTable = (atividade) => {

        ReactSwal.fire({
            title: `Deseja excluir o atividade ${atividade.name}?`,
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`,
        })
            .then((result) => {
                if (result.isConfirmed) {
                    dispatch(removeAtividade(atividade.id))
                        .then(() => {
                            ReactSwal.fire({
                                icon: 'success',
                                title: `O Atividade ${atividade.name} foi deletado com sucesso !`,
                                showConfirmButton: false,
                                showCloseButton: true,
                            })
                        })
                }
            })

    }

    const submitForm = () => {
        const nForm = {
            ...form,
            start_date: formatDate(form.start_date)
        }

        dispatch(isUpdate ? updateAtividade(nForm) : createAtividade(nForm))

            .then(() => {

                setForm({});
                toggle();

                ReactSwal.fire({
                    icon: 'success',
                    title: `Atividade ${form.name} cadastrado com sucesso !`,
                    showConfirmButton: false,
                    showCloseButton: true,
                    timer: 4000,
                })

            })

    }

    useEffect(() => {
        const nDetalhe = {
            ...detalhe,
            start_date: moment(detalhe.start_date).format('YYYY-MM-DD')
        }
        setForm({ ...nDetalhe })

    }, [detalhe, setForm])

    return (
        <React.Fragment>
            <TitlePage>
                Atividades
              <Button onClick={toggle} size="sm" color="info">Cadastrar</Button>
            </TitlePage>

            <TableList atividades={atividades} editarAtividade={editTable} excluirAtividade={deleteTable} />



            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>
                    {isUpdate ? "Atualizar" : "Cadastrar"} Atividade

                </ModalHeader>
                <ModalBody>
                    <FormAtividade state={stateForm} />
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={submitForm}>
                        {isUpdate ? "Atualizar" : "Cadastrar"}
                    </Button>
                </ModalFooter>
            </Modal>

        </React.Fragment>
    )
}


export default GerenciarAtividades;



// utils
const formatDate = (data) => {
    const [y, m, d] = data.split('-')
    return `${d}/${m}/${y}`
}
