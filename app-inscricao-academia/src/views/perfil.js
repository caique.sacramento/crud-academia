import { useState } from "react"
import { Button, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import { TitlePage } from '../assets/styled';
import { useDispatch, useSelector } from 'react-redux';
import styled from "styled-components";
import moment from 'moment'
import { updateProfile } from '../store/aluno/aluno.action';


const Perfil = () => {
    document.title = 'CRUD Academia - perfil'
    // antes de preencher os dados do perfil, 
    // faça um get (corrigir no Back) do perfil e preencha o formulário
    const dispatch = useDispatch();

    const perfil = useSelector(state => state.auth.usuario)

    const [form, setForm] = useState({
        ...perfil,
        datanascimento: moment(perfil.datanascimento).format('YYYY-MM-DD')
    })

    const handleChange = (e) => {

        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const updateForm = () => {

        const dt = form.datanascimento.split('-')

        const nPerfil = {
            ...form,
            datanascimento: `${dt[2]}/${dt[1]}/${dt[0]}`
        }
        delete nPerfil.tipo;

        dispatch(updateProfile(nPerfil))

    }

    return (
        <>
            <TitlePage>Perfil</TitlePage>

            <BoxInscricao>
                <Col xs="12" sm="12" md="8" lg="8">
                    <FormGroup>
                        <Label for="name">Nome</Label>
                        <Input type="text" id="name" value={form.nome || ""} onChange={handleChange}
                            name="nome" placeholder="Insira seu nome" className="text-uppercase" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="email" id="email" value={form.email || ""} onChange={handleChange}
                            name="email" placeholder="Insira seu email" className="text-lowercase" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="nascimento">Data Nascimento</Label>
                        <Input type="date" id="nascimento" value={form.datanascimento || ""} onChange={handleChange}
                            name="data_nascimento" placeholder="Insira seu nascimento" />
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" onClick={updateForm}>Atualizar</Button>
                    </FormGroup>
                </Col>
            </BoxInscricao>

        </>
    )
}


export default Perfil;

const BoxInscricao = styled(Row)`
`