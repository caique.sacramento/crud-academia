import { useEffect, useState } from 'react';
import { TitlePage } from "../assets/styled";
import { Table, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { getStudentAll } from '../store/aluno/aluno.action';
import { BsListTask } from 'react-icons/bs';




const Alunos = () => {
    document.title = 'CRUD Academia - atividades'
    const dispatch = useDispatch()
    const alunos = useSelector(state => state.aluno.all)
    const [modal, setModal] = useState({
        status: false,
        data: {}
    });

    const toggle = (data = {}) => setModal({
        status: !modal.status,
        data: data
    })


    useEffect(() => {
        dispatch(getStudentAll());
    }, [dispatch])

    return (
        <>
            <TitlePage>
                Alunos
            </TitlePage>


            <Table>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {alunos?.map((aluno, i) => (
                        <tr key={i}>
                            <td>{aluno.nome}</td>
                            <td>{aluno.nome}</td>
                            <td>{aluno.inscricoes.length > 0 ? (<div onClick={() => toggle(aluno)} style={{ cursor: 'pointer' }}><BsListTask /></div>) : ""} </td>
                        </tr>
                    ))}
                </tbody>
            </Table>

            <Modal isOpen={modal.status} toggle={toggle} >
                <ModalHeader toggle={toggle}>Lista de Alunos</ModalHeader>
                <ModalBody>
                    <Table>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Atividade</th>
                            </tr>
                        </thead>
                        <tbody>
                            {modal.data?.inscricoes?.map((v, i) => (
                                <tr key={i}>
                                    <td>{i}</td>
                                    <td>{v.atividade}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </ModalBody>

            </Modal>

        </>
    )
}


export default Alunos;