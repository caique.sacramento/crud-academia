import { useEffect } from "react";
import { useParams } from "react-router";
import { Jumbotron, Button } from 'reactstrap';
import Loading from '../components/loading'
import styled from "styled-components";
import { AiFillCheckSquare, AiFillCloseSquare } from 'react-icons/ai'
import Tabela from '../components/tabela'
import moment from 'moment'
import { useDispatch, useSelector } from "react-redux";
import { getDetails } from "../store/atividade/atividade.action";
import { createSubscription, removeSubscription } from "../store/aluno/aluno.action";
import ReactSwal from "../plugins/swal";


const Detalhes = (props) => {
    const { id } = useParams();
    const dispatch = useDispatch();

    const isAdmin = useSelector(state => state.auth.isAdmin)
    const detalhe = useSelector(state => state.atividade.details)
    const registered = useSelector(state => state.atividade.details.registered)
    const loading = useSelector(state => state.atividade.loading)

    const inscricoes = useSelector(state => state.atividade.details.inscricoes)


    const toogleSubcription = (inscricoes) => {
        if (registered) {
            dispatch(removeSubscription(id, inscricoes[0].id))
                .then(() => {
                    ReactSwal.fire({
                        icon: 'success',
                        title: `Aluno Removido do Atividade`,
                        showConfirmButton: false,
                        showCloseButton: true,
                    })
                })
                .catch(erro => console.log('deu ruim...'))
        } else {
            dispatch(createSubscription(id))
                .then(() => {
                    ReactSwal.fire({
                        icon: 'success',
                        title: `Aluno Cadastrado com sucesso !`,
                        showConfirmButton: false,
                        showCloseButton: true,
                    })
                })
                .catch(erro => console.log('deu ruim...'))
        }


    }

    useEffect(() => {
        dispatch(getDetails(id))
    }, [dispatch, id])


    const Detalhamento = ({ name, professor, start_date }) => (
        <Jumbotron style={{ backgroundColor: registered && !isAdmin ? '#D4EDDA' : '#eee' }}>
            <div className="display-4">{name}</div>
            <div className="lead">
                <strong>Cordenador:</strong> {professor}
            </div>
            <div>
                <strong>Data de Início:</strong> {moment(start_date).format('DD/MM/YYYY')}
            </div>

        </Jumbotron>
    )


    const Menu = () => (
        <Navbar expand="md mb-4">
            <Button onClick={() => toogleSubcription(inscricoes)} color={!registered ? "primary" : "secondary"} size="sm">
                {!registered ? (<><AiFillCheckSquare /> Inscreva-se </>) : (<><AiFillCloseSquare /> Remover Inscrição</>)}
            </Button>
        </Navbar>
    )

    const montarTela = (detalhe) => (
        <div>
            {Detalhamento(detalhe)}
            {!isAdmin ? Menu() : <Tabela inscritos={detalhe.inscricoes} />}

        </div>
    )

    return (
        loading
            ? <Loading />
            : montarTela(detalhe)

    )
}


export default Detalhes;


const Navbar = styled.div`
    background-color:none !important;
    margin: 10px 0 20px;import { createSubscription } from '../store/aluno/aluno.action';

    padding: 10px 0;
    border-bottom: thin dotted #4446;
    display:flex;
    
    .info {
        flex:1;
    }


`

// tela de detalhes

// Menu de toogle de troca de tela (Botão)
// tela de inscrição / tabela