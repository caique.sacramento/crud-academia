import React, { useEffect } from 'react';
import CardItem from "../components/atividades/card_item";
import Loading from '../components/loading'
import styled from 'styled-components';
import { Col, Row } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { getatividadeAll } from '../store/atividade/atividade.action';

const Atividades = () => {
    document.title = 'CRUD Academia - inicio'
    const dispatch = useDispatch()
    // const [loading, setLoading] = useState(false)
    const atividades = useSelector(state => state.atividade.all)
    const loading = useSelector(state => state.atividade.loading)

    useEffect(() => {
        dispatch(getatividadeAll());
    }, [dispatch])

    const MapearAtividades = (atividades) => atividades.map((item, i) => (
        <Col md="3" xl="3" sm="12" xs="12" key={i} className="mb-4">
            <CardItem item={{ ...item, status: true }} />
        </Col>
    ))

    if (loading) {
        return <Loading />
    }

  

    return (
        <>
            <BoxAtividades>
                {!loading && atividades.length === 0 ? "Não tem atividades disponiveis" : MapearAtividades(atividades)}
                {/* {atividades.length == 0 ? "Não tem atividades disponiveis" : loading ? <Loading /> : MapearAtividades(atividades)} */}

            </BoxAtividades>

        </>
    )
}

export default Atividades;


const BoxAtividades = styled(Row)`

`