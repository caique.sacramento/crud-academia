import http from '../config/http';


const getServiceAllAtividades = () => http.get('/atividade');

const getServiceDetalhe = (id) => http.get(`/atividade/${id}`);

const createServiceAtividade = (atividade) => http.post(`atividade`, atividade);

const updateServiceAtividade = (id, data) => http.put(`atividade/${id}`, data);

const removeServiceAtividade = (id) => http.delete(`atividade/${id}`);

const subServiceAtividade = (id, data) => http.post(`atividade/${id}/inscricao`, data);

const deleteServiceSubscription = (id_atividade, id_inscricao) => http.delete(`/atividade/${id_atividade}/inscricao/${id_inscricao}`);



export {
    getServiceAllAtividades,
    getServiceDetalhe,
    createServiceAtividade,
    removeServiceAtividade,
    subServiceAtividade,
    deleteServiceSubscription,
    updateServiceAtividade
}
