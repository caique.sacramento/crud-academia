import http from '../config/http';



const getServiceStudentAll = () => http.get('/aluno');
const getServiceStudent = (id) => http.get(`/aluno/${id}`);
const createSubStudentInAtividade = (id) => http.post(`/atividade/${id}/inscricao`);
const removeSubStudentInAtividade = (id_atividade, id_inscricao) => http.delete(`/atividade/${id_atividade}/inscricao/${id_inscricao}`);
const putServiceStudent = (id, data) => http.put(`/aluno/${id}`, data);


export {
    getServiceStudentAll,
    createSubStudentInAtividade,
    removeSubStudentInAtividade,
    putServiceStudent,
    getServiceStudent
}
