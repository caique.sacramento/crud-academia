import React from "react";
import {
    Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
// config
import { isAuthenticated } from './config/auth'
// layout
import Layout from './components/layout'
// views
import Inicio from './views/inicio';
import Atividades from './views/atividades';
import Alunos from './views/alunos';
import Version from './views/version';
import Detalhes from './views/detalhes';
import Error404 from './views/errors/404';
import Error401 from './views/errors/401';
import history from './config/history';
import SignUp from "./views/auth/signup";
import SignIn from './views/auth/signin';
import Perfil from './views/perfil'

import { useSelector } from 'react-redux';

const AdminRoute = ({ ...rest }) => {
    if (!isAuthenticated()) {
        return <Redirect to="/signin" />
    }

    const hasAdmin = Object.keys(rest).includes('admin') && !rest.admin

    if (hasAdmin) {
        return <Redirect to="/error/401" />
    }

    return <Route {...rest} />
}


const Routers = () => {
    const isAdmin = useSelector(state => state.auth.isAdmin)

    return (
        <Router history={history}>
            <Layout nomeDaPagina="CRUD Academia">
                <Switch>
                    <Route exact path='/signin' component={SignIn} />
                    <Route exact path='/signup' component={SignUp} />

                    <AdminRoute exact path='/' component={Inicio} />
                    <AdminRoute exact path='/version' component={Version} />
                    <AdminRoute exact path='/detalhes/:id' component={Detalhes} />
                    <AdminRoute exact path='/perfil' component={Perfil} />

                    {/* ADMIN */}
                    <AdminRoute exact path='/alunos' admin={isAdmin} component={Alunos} />
                    <AdminRoute exact path='/atividades' admin={isAdmin} component={Atividades} />


                    <AdminRoute exact to="/error/401" component={Error401} />
                    <AdminRoute exact to="/error/404" component={Error404} />
                    <Redirect from="*" to="/error/404" />
                    {/* <Route component={Error404} /> */}
                </Switch>
            </Layout>
        </Router >
    )

}


export default Routers
