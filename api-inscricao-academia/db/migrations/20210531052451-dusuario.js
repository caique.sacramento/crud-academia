'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    return Promise.all([

      queryInterface
        .addColumn('inscricoes', 'usuario_id', Sequelize.INTEGER, {
          allowNull: true,
        }),

      queryInterface
        .addConstraint('inscricoes', {
          fields: ['usuario_id'],
          type: 'foreign key',
          name: 'inscricao_usuario_fk',
          references: {
            table: 'usuarios',
            field: 'id'
          },
          onDelete: 'cascade',
        }),
    ]);

  },
  down: async (queryInterface, Sequelize) => {

    return Promise.all([
      queryInterface.removeConstraint('inscricoes', 'inscricao_usuario_fk'),
      queryInterface.removeColumn('inscricoes', 'usuario_id'),
    ]);

  }
};