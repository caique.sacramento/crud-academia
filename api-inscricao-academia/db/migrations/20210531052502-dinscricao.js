'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    return Promise.all([

      queryInterface.changeColumn('inscricoes', 'name', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      }),

      queryInterface.changeColumn('inscricoes', 'email', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      }),

      queryInterface.changeColumn('inscricoes', 'data_nascimento', {
        type: Sequelize.DATE
      }, {
        allowNull: true,
      }),

    ]);
  },

  down: async (queryInterface, Sequelize) => {

    return Promise.all([

      queryInterface.changeColumn('inscricoes', 'name', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      }),

      queryInterface.changeColumn('inscricoes', 'email', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      }),

      queryInterface.changeColumn('inscricoes', 'data_nascimento', {
        type: Sequelize.DATE
      }, {
        allowNull: false,
      }),

    ]);
  }
};

