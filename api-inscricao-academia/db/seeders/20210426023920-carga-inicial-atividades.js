'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('atividades',
      [
        {
          professor: 'Julio',
          name: 'Musculação',
          start_date: '2021-04-25',
          status: true,
        },
        {
          professor: 'Caique',
          name: 'Crossfit',
          start_date: '2021-04-25',
          status: true,
        },
        {
          professor: 'Murilo',
          name: 'Boxe',
          start_date: '2021-04-25',
          status: true,
        },
        {
          professor: 'Silva',
          name: 'Funcional',
          start_date: '2021-04-25',
          status: true,
        },
        {
          professor: 'Sacramento',
          name: 'Abdominal',
          start_date: '2021-04-25',
          status: true,
        },
        {
          professor: 'Mauricio',
          name: 'Spinning',
          start_date: '2021-04-25',
          status: true,
        },
      ],
      {
        updateOnDuplicate: ['name', 'professor', 'start_date', 'status'],
        ignoreDuplicates: true
      }
    )

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('atividades', null, {});
  }
};