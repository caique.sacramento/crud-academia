module.exports = (sequelize, DataTypes) => {
    const inscricoes = sequelize.define(
        "inscricoes",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
                allowNull: false
            },
            name: DataTypes.TEXT,
            email: DataTypes.TEXT,
            data_nascimento: DataTypes.DATE
        },
        {
            underscored: true,
            paranoid: true,
            timestamps: false
        }
    );

    inscricoes.associate = function (models) {
        inscricoes.belongsTo(models.atividades, {
            foreignKey: 'atividade_id',
            as: 'atividades'
        });

        inscricoes.belongsTo(models.usuarios, {
            foreignKey: 'usuario_id',
            as: 'usuario'
        })
    };

    return inscricoes;

};