module.exports = (sequelize, DataTypes) => {
    const atividades = sequelize.define(
      "atividades",
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
          allowNull: false
        },
        professor: DataTypes.TEXT,
        name: DataTypes.TEXT,
        start_date: DataTypes.TEXT,
        status: DataTypes.BOOLEAN,
      },
      {
        underscored: true,
        paranoid: true,
        timestamps: false
      }
    );
  
    atividades.associate = function (models) {
  
      atividades.hasMany(models.inscricoes, {
        foreignKey: 'atividade_id',
        as: 'inscricoes'
      });
    };
  
    return atividades;
  
  };