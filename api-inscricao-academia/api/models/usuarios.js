
module.exports = (sequelize, DataTypes) => {

    const usuarios = sequelize.define("usuarios", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
      },
      nome: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      email: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      tipo: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      datanascimento: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      senha: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
    }, {
      underscored: true,
      paranoid: true,
      timestamps: false
    });
  
    usuarios.associate = function (models) {
  
      usuarios.hasMany(models.inscricoes, {
        foreignKey: 'usuario_id',
        as: 'inscricoes'
      });
    };
  
    return usuarios;
  
  }