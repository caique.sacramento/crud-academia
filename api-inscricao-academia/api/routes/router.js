const { Router } = require("express")
const { name, version } = require('../../package.json')
const atividadeRoutesV1 = require('../routes/v1/atividade')

const atividadeRoutesV2 = require('../routes/v2/atividade');
const usuarioRoutesV2 = require('./v2/usuario');

module.exports = (app) => {
    app.get('/', (req, res, next) => {
        res.send({ name, version });
      });
      const routerV1 = Router();
      atividadeRoutesV1(routerV1);
      app.use('/v1', routerV1);
    
      const routerV2 = Router();
      atividadeRoutesV2(routerV2);
      usuarioRoutesV2(routerV2);
      app.use('/v2', routerV2);
}