const atividadesController = require('../../controllers/atividade.controller')

module.exports = (router) => {

  router
    .route('/atividade')
    .get(
      atividadesController.getAtividades
    )

  router
    .route('/atividade/:atividadeid')
    .get(
      atividadesController.getAtividadePorId
    )


  router
    .route('/atividade/:atividadeid/inscricao')
    .post(
      atividadesController.postAtividadeInscricao
    )


  router
    .route('/atividade/:atividadeid/inscricao/:idinscricao')
    .delete(
      atividadesController.deleteAtividadeInscricao
    )
}