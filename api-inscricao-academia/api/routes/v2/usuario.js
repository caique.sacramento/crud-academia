const usuarioController = require('../../controllers/usuario.controller');
const { validarDTO, autorizar } = require('../../utils/middlewares.utils');

const Joi = require('joi').extend(require('@joi/date'));

module.exports = (router) => {

  router
    .route('/auth')
    .post(

      validarDTO('body', {
        senha: Joi.string().required().messages({
          'any.required': `"senha" é um campo obrigatório`,
          'string.empty': `"nome" não deve ser vazio`,
        }),
        usuario: Joi.string().required().messages({
          'any.required': `"usuario" é um campo obrigatório`,
          'string.empty': `"usuario" não deve ser vazio`,
        }),
      }),

      usuarioController.autenticar
    );

  router
    .route('/aluno')
    .get(
      autorizar('LISTAR_ATIVIDADE'),
      usuarioController.listarAlunos
    )
    .post(

      validarDTO('body', {
        nome: Joi.string().min(5).max(30).required()
          .messages({
            'any.required': `"nome" é um campo obrigatório`,
            'string.empty': `"nome" não deve ser vazio`,
            'string.min': `"nome" não deve ter menos que {#limit} caracteres`,
            'string.max': `"nome" não deve ter mais que {#limit} caracteres`,
          }),
        email: Joi.string().email().required().messages({
          'any.required': `"email" é um campo obrigatório`,
          'string.empty': `"email" não deve ser vazio`,
          'string.email': `"email" deve ser um email válido`,
        }),
        datanascimento: Joi
          .date().format('DD/MM/YYYY')
          .required()
          .messages({
            'any.required': `"datanascimento" é um campo obrigatório`,
            'date.format': `"datanascimento" deve ser uma data válida "{#format}"`,
          }),
        senha: Joi
          .string()
          .required()
          .min(6)
          .max(16)
          .messages({
            'any.required': `"senha" é um campo obrigatório`,
            'string.empty': `"senha" não deve ser vazio`,
            'string.min': `"senha" não deve ter menos que {#limit} caracteres`,
            'string.max': `"senha" não deve ter mais que {#limit} caracteres`,
          })
      }),
      usuarioController.criaAluno
    );

  router
    .route('/aluno/:id')
    .get(
      autorizar('LISTAR_ATIVIDADE'),
      usuarioController.pesquisaAlunoPorId
    )
    .put(
      autorizar('ALTERACAO_ALUNO'),
      validarDTO('params', {
        id: Joi.number().integer().required().messages({
          'any.required': `"id" é um campo obrigatório`,
          'number.base': `"id" deve ser um número`,
          'number.integer': `"id" deve ser um número válido`
        })
      }),
      validarDTO('body', {
        nome: Joi.string().min(5).max(30).required()
          .messages({
            'any.required': `"nome" é um campo obrigatório`,
            'string.empty': `"nome" não deve ser vazio`,
            'string.min': `"nome" não deve ter menos que {#limit} caracteres`,
            'string.max': `"nome" não deve ter mais que {#limit} caracteres`,
          }),
        email: Joi.string().email().required().messages({
          'any.required': `"email" é um campo obrigatório`,
          'string.empty': `"email" não deve ser vazio`,
          'string.email': `"email" deve ser um email válido`,
        }),
        datanascimento: Joi
          .date().format('DD/MM/YYYY')
          .required()
          .messages({
            'any.required': `"datanascimento" é um campo obrigatório`,
            'date.format': `"datanascimento" deve ser uma data válida "{#format}"`,
          })
      }),
      usuarioController.alteraAluno
    )


  router
    .route('/usuario')
    .get(
      autorizar('LISTAR_ATIVIDADE'),
      usuarioController.listarUsuarios
    );

}
