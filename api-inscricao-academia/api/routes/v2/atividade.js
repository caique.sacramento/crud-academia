
const atividadesController = require('../../controllers/atividade.controller');

const { autorizar, validarDTO } = require('../../utils/middlewares.utils');

const Joi = require('joi').extend(require('@joi/date'));

module.exports = (router) => {

  router
    .route('/atividade')
    .get(
      autorizar(),
      atividadesController.listarAtividades,
    )
    .post(
      autorizar('CRIACAO_ATIVIDADE'),
      validarDTO('body', {
        professor: Joi.string().min(5).required(),
        name: Joi.string().min(5).required(),
        start_date: Joi
          .date().format('DD/MM/YYYY')
          .required(),
      }),
      atividadesController.criaAtividade);

  router
    .route('/atividade/:atividadeid')
    .get(
      autorizar(),
      validarDTO("params", {
        atividadeid: Joi.number().integer().required().messages({
          'any.required': `"id" é um campo obrigatório`,
          'number.base': `"id" deve ser um número`,
          'number.integer': `"id" deve ser um número válido`
        })
      }),
      atividadesController.listarAtividadePorId,
    );

  router
    .route('/atividade/:atividadeid')
    .put(
      autorizar('ALTERACAO_ATIVIDADE'),
      validarDTO("params", {
        atividadeid: Joi.number().integer().required().messages({
          'any.required': `"id" é um campo obrigatório`,
          'number.base': `"id" deve ser um número`,
          'number.integer': `"id" deve ser um número válido`
        })
      }),
      validarDTO("body", {
        name: Joi.string().required().messages({
          'any.required': `"nome" é um campo obrigatório`,
        }),
        professor: Joi.string().required().messages({
          'any.required': `"professor" é um campo obrigatório`,
        }),
        start_date: Joi
          .date().format('DD/MM/YYYY')
          .required()
          .messages({
            'any.required': `"start_date" é um campo obrigatório`,
            'date.format': `"start_date" deve ser uma data válida "{#format}"`,
          }),
      }),
      atividadesController.atualizaAtividade
    )
    .delete(
      autorizar('DELECAO_ATIVIDADE'),
      validarDTO("params", {
        atividadeid: Joi.number().integer().required().messages({
          'any.required': `"atividadeid" é um campo obrigatório`,
          'number.base': `"atividadeid" deve ser um número`,
          'number.integer': `"atividadeid" deve ser um número válido`
        })
      }),
      atividadesController.deletarAtividade,
    )

  router
    .route('/atividade/:atividadeid/inscricao')
    .post(
      autorizar('REALIZAR_INSCRICAO'),
      validarDTO("params", {
        atividadeid: Joi.number().integer().required().messages({
          'any.required': `"atividadeid" é um campo obrigatório`,
          'number.base': `"atividadeid" deve ser um número`,
          'number.integer': `"atividadeid" deve ser um número válido`
        })
      }),
      atividadesController.realizaInscricao
    );

  router
    .route('/atividade/:atividadeid/inscricao/:inscricaoid')
    .delete(
      autorizar('DELECAO_INSCRICAO'),
      validarDTO("params", {
        atividadeid: Joi.number().integer().required().messages({
          'any.required': `"atividadeid" é um campo obrigatório`,
          'number.base': `"atividadeid" deve ser um número`,
          'number.integer': `"atividadeid" deve ser um número válido`
        }),
        inscricaoid: Joi.number().integer().required().messages({
          'any.required': `"inscricaoid" é um campo obrigatório`,
          'number.base': `"inscricaoid" deve ser um número`,
          'number.integer': `"inscricaoid" deve ser um número válido`
        })
      }),
      atividadesController.removeInscricao
    );
}