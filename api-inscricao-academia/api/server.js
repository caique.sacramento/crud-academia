const express = require('express')
const app = express()
const cors = require('cors')
const router = require('./routes/router')

app.use(express.json())
app.use(cors())

router(app)

const PORT = 3001

app.listen(PORT, () => {
    // console.log(`server listening on port: ${PORT}`) já está sendo informado no index que está rodando
})

module.exports = app