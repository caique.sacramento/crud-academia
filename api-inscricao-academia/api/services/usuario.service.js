const md5 = require('md5');
const jwt = require('jsonwebtoken');
const hashSecret = process.env.CRYPTO_KEY;
const { usuarios, inscricoes, atividades } = require("../models");

const buscarPorEmail = async (email) => {
  return usuarios.findOne({
    where: {
      email: email
    },
  });
}

const criarHash = (senha) => {
  return md5(senha + hashSecret);
}

const usuarioExiste = async (usuario, senha) => {
  const usuarioFromDB = await usuarios.findOne({
    where: {
      email: usuario,
      senha: criarHash(senha)
    },
  });
  return usuarioFromDB ? true : false;
};

const criaCredencial = async (usuarioEmail) => {

  try {

    const usuario = await usuarios.findOne({
      where: {
        email: usuarioEmail
      },
    });

    const { id, nome, email, tipo, datanascimento } = usuario;

    const credencial = {
      token: jwt.sign({ email: usuario.email }, process.env.JWT_KEY, {
        expiresIn: `${process.env.JWT_VALID_TIME}ms`,
      }),
      usuario: {
        id,
        nome,
        email,
        tipo,
        datanascimento
      }
    }

    return credencial;

  } catch (error) {
    console.log(error);
  }
}

const validaSeEmailJaExiste = async (email, id = 0) => {

  const resul = await buscarPorEmail(email);


  if (id === 0) {
    return resul ? true : false;
  }


  if (resul) {

    if (resul.id === id)
      return false;

    return true;


  } else {
    return false;

  }

}

const criaAluno = async (model) => {

  const modelParaCadastro = {
    nome: model.nome,
    email: model.email,
    tipo: '2',
    datanascimento: model.datanascimento,
    senha: criarHash(model.senha),
  };

  return usuarios.create(modelParaCadastro);

}

const alteraAluno = async (id, model) => {
  return usuarios.update(
    {
      nome: model.nome,
      email: model.email,
      datanascimento: model.datanascimento,
    },
    {
      where: { id: id }
    }
  )
}

const pesquisaAlunoPorId = async (idAluno) => {

  const itemDB = await usuarios.findOne({
    where: {
      id: idAluno
    },
    include: {
      model: inscricoes,
      as: 'inscricoes',
      include: {
        model: atividades,
        as: 'atividade',
      }
    }
  });

  const { id, nome, email, datanascimento } = itemDB;

  const resultInscricoes = itemDB.inscricoes.map(itemInscricao => {

    const { id, atividade } = itemInscricao;

    return {
      id,
      atividade: {
        id: atividade.id,
        name: atividade.name
      }
    }

  });

  return {
    id,
    nome,
    email,
    datanascimento,
    qtd_inscricoes: resultInscricoes.length,
    inscricoes: resultInscricoes
  }
}

const listar = async (tipo) => {

  let where = {};

  if (tipo)
    where = {
      tipo
    }

  const resultFromDB = await usuarios.findAll({
    where,
    include: [
      {
        model: inscricoes,
        as: 'inscricoes',
        include: [
          {
            model: atividades,
            as: 'atividade'
          }
        ]
      }
    ],
  });

  return resultFromDB;
}

const listarAlunos = async () => {

  const resultFromDB = await listar('2');

  return resultFromDB.map(item => {

    const { id, email, nome, tipo, inscricoes } = item;

    return {
      id,
      nome,
      email,
      tipo,
      inscricoes: inscricoes.reduce((acc, item) => {
        const { id, atividade } = item;
        const novoItem = { id, atividade: atividade.name, };
        return [...acc, novoItem]
      }, []),
    }
  });

}

const listarTodos = async () => {

  const resultFromDB = await listar();

  return resultFromDB.map(item => {

    const { id, email, nome, tipo, inscricoes } = item;

    return {
      id,
      nome,
      email,
      tipo,
      inscricoes: inscricoes.reduce((acc, item) => {
        const { id, atividade } = item;
        const novoItem = { id, atividade: atividade.name, };
        return [...acc, novoItem]
      }, []),

    }
  });
}

module.exports = {
  usuarioExiste,
  criaAluno,
  criaCredencial,
  validaSeEmailJaExiste,
  buscarPorEmail,
  alteraAluno,
  listarAlunos,
  pesquisaAlunoPorId,
  listarTodos,
}