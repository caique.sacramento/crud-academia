const { atividades, inscricoes, usuarios, Sequelize } = require("../models");


const listarPorPerfilUsuario = async (usuarioTipo, usuarioId) => {

  const resultFromDB = await atividades.findAll({
    include: [
      {
        model: inscricoes,
        as: 'inscricoes',
      }
    ]
  });

  let result = [];

  if (Number(usuarioTipo) === 2) {


    result = resultFromDB.map((item) => {

      const { id, name, start_date, status, inscricoes } = item;

      const filterResult = inscricoes.filter((itemFilter) => {
        return Number(itemFilter.usuario_id) === Number(usuarioId)
      });

      return {
        id,
        name,
        start_date,
        status,
        inscrito: filterResult.length > 0 ? true : false
      };

    });

  } else {

    result = resultFromDB.map(item => {

      const { id, name, start_date, status, inscricoes } = item;

      return {
        id,
        name,
        start_date,
        status,
        qtd_inscricoes: inscricoes.length
      }

    });

  }

  return result;
}

const pesquisarAtividadePorPerfilUsuario = async (id, usuarioId, usuarioTipo) => {

  const resultFromDB = await atividades.findOne({
    where: {
      id: id
    },
    include: [
      {
        model: inscricoes,
        as: 'inscricoes',
        include: [
          {
            model: usuarios,
            as: 'usuario',
          }
        ]
      },
    ]
  });

  let inscricoesFiltered = resultFromDB.inscricoes;

  if (Number(usuarioTipo) === 2) {
    inscricoesFiltered = inscricoesFiltered.filter(item => item.usuario_id === usuarioId);
  }

  const inscricoesFinal = inscricoesFiltered.map((itemInscricao) => {
    return {
      id: itemInscricao.id,
      aluno: {
        id: itemInscricao.usuario.id,
        nome: itemInscricao.usuario.nome,
        email: itemInscricao.usuario.email,
        datanascimento: itemInscricao.usuario.data_nascimento,
      }
    }
  });

  return {
    id: resultFromDB.id,
    name: resultFromDB.name,
    professor: resultFromDB.professor,
    start_date: resultFromDB.start_date,
    status: resultFromDB.status,
    inscricoes: inscricoesFinal
  }

}

const validaSeNomeJaExiste = async (nome) => {

  var resultFromDB = await atividades.findOne({
    where: {
      name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', nome.toLowerCase())
    }
  });

  return resultFromDB ? true : false;

}

const criaAtividade = async (body) => {
  return atividades.create({
    name: body.name,
    professor: body.professor,
    start_date: `${body.start_date}`
  });
}

const alteraAtividade = async (id, body) => {

  const model = {
    name: body.name,
    professor: body.professor,
    start_date: body.start_date,
  }

  return atividades.update(
    { ...model },
    { where: { id: id } }
  )

}

const deletaAtividade = async (id) => {

  return atividades.destroy({
    where: {
      id: id
    }
  });

}

const validaSeAtividadeExiste = async (id) => {

  const resultFromDB = await atividades.findOne({
    where: {
      id
    }
  })

  return resultFromDB ? true : false;

}

const addInscricaoAluno = async (atividadeId, usuarioId) => {

  return inscricoes.create({
    atividade_id: atividadeId,
    usuario_id: usuarioId,
  });

}

module.exports = {
  addInscricaoAluno,
  alteraAtividade,
  criaAtividade,
  deletaAtividade,
  validaSeNomeJaExiste,
  validaSeAtividadeExiste,
  listarPorPerfilUsuario,
  pesquisarAtividadePorPerfilUsuario,
}