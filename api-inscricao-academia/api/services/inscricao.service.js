const { inscricoes, } = require("../models");


const validaSeInscricaoExiste = async (inscricaoid) => {

  const resultFromDB = await inscricoes.findOne({
    where: {
      id: inscricaoid
    }

  })

  return resultFromDB ? true : false;

}

const validaSeInscricaoPertenceAoUsuario = async (inscricaoId, usuarioId) => {

  const resultFromDB = await inscricoes.findOne({
    where: {
      id: inscricaoId
    }
  });

  return resultFromDB.usuario_id === usuarioId ? true : false;

}

const validaSeUsuarioJaInscrito = async (atividadeId, usuarioId) => {

  const resultFromDB = await inscricoes.findOne({
    where: {
      atividade_id: atividadeId,
      usuario_id: usuarioId
    }
  });

  return resultFromDB ? true : false;

}

const removerInscricao = async (atividadeId) => {

  return inscricoes.destroy({
    where: {
      id: atividadeId
    }
  });
}

module.exports = {
  removerInscricao,
  validaSeUsuarioJaInscrito,
  validaSeInscricaoExiste,
  validaSeInscricaoPertenceAoUsuario,
}