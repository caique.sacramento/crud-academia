
const usuarioService = require('../services/usuario.service');

const autenticar = async (req, res, next) => {

  try {

    const { usuario, senha } = req.body;

    const result = await usuarioService.usuarioExiste(usuario, senha);

    if (!result) {
      return res.status(401).send({
        mensagem: 'usuário ou senha inválidos'
      })
    }

    var credencial = await usuarioService.criaCredencial(usuario);

    return res.status(200).send(credencial);

  } catch (error) {

    console.log(error);

    res.status(500).send({
      mensagem: "internal server error",
    });

  }

}

const criaAluno = async (req, res, next) => {

  try {

    const { body } = req;

 
    const validacaoEmail = await usuarioService.validaSeEmailJaExiste(body.email);

    console.log(validacaoEmail);

    if (validacaoEmail) {
      return res.status(400).send({
        detalhes: [
          `"email" já cadastrado.`,
        ]
      });
    }

    await usuarioService.criaAluno(body);

    return res.status(200).send({
      mensagem: 'cadastro realizado com sucesso',
    });

  } catch (error) {

    console.log(error);

    res.status(500).send({
      mensagem: "ERROR!!",
    });
  }

}

const alteraAluno = async (req, res, next) => {

  const { body, params } = req;

  console.log('req.usuario.id: ', req.usuario.id);
  console.log('params.id: ', params.id);
  if (Number(params.id) !== Number(req.usuario.id))
    return res.status(400).send({
      mensagem: `"operação nao permitida.`,
    });

  const validacaoEmail = await usuarioService.validaSeEmailJaExiste(body.email, params.id);

  if (validacaoEmail) {
    return res.status(400).send({
      mensagem: `"email" já cadastrado.`,
    });
  }

  await usuarioService.alteraAluno(params.id, body);


  return res.status(200).send({
    mensagem: 'Alteração realizada com sucesso',
  });

}

const pesquisaAlunoPorId = async (req, res, next) => {

  try {
    const { params } = req;

    const user = await usuarioService.pesquisaAlunoPorId(params.id);

    return res.status(200).send(user);

  } catch (error) {

    console.log(error);

    return res.status(500).send({
      mensagem: "internal server error",
    });

  }

}

const listarAlunos = async (req, res, next) => {

  const alunos = await usuarioService.listarAlunos();

  return res.status(200).send(alunos);

}

const listarUsuarios = async (req, res, next) => {

  const usuarios = await usuarioService.listarTodos();

  return res.status(200).send(usuarios);

}

module.exports = {
  autenticar,
  criaAluno,
  alteraAluno,
  listarAlunos,
  pesquisaAlunoPorId,
  listarUsuarios
}