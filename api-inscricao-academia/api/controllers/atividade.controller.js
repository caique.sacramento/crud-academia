
const { atividades, inscricoes } = require("../models");

const atividadesServices = require('../services/atividade.service');
const inscricoesServices = require('../services/inscricao.service');

const getAtividades = async (req, res, next) => {

  const result = await atividades.findAll({});

  res.status(200).send(result.map(item => {

    const { id, name, ...resto } = item;

    return {
      id,
      name
    }
  }) || []);
}

const getAtividadePorId = async (req, res, next) => {

  try {
    const { atividadeid } = req.params

    const result = await atividades.findOne({
      where: {
        id: atividadeid
      },
      include: {
        model: inscricoes,
        as: 'inscricoes',
      },
    });

    const data = {
      id: result.id,
      name: result.name,
      professor: result.professor,
      status: result.status,
      subscriptions: result.inscricoes,
    }

    res.status(200).send(data);

  } catch (error) {

    res.status(500).send({ message: 'Internal server error!!' });
  }
}

const postAtividadeInscricao = async (req, res, next) => {

  try {

    const { atividadeid } = req.params;

    const body = req.body;

    const model = {
      atividade_id: atividadeid,
      name: body.name,
      email: body.email,
      data_nascimento: body.name,
    }

    await inscricoes.create(model);

    res.status(200).send({ message: 'iniscrição incluída com sucesso!' });

  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Internal server error!!' });
  }
}

const deleteAtividadeInscricao = async (req, res, next) => {
  try {

    const { idinscricao } = req.params;

    await inscricoes.destroy({
      where: {
        id: idinscricao,
      }
    });

    res.status(200).send({ message: 'iniscrição excluída com sucesso!' });

  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Internal server error!!' });
  }
}

// V2

const listarAtividades = async (req, res, next) => {

  const result = await atividadesServices.listarPorPerfilUsuario(req.usuario.tipo, req.usuario.id);

  return res.status(200).send(result);

}

const listarAtividadePorId = async (req, res, next) => {


  try {

    const { params, usuario } = req;

    const result = await atividadesServices.pesquisarAtividadePorPerfilUsuario(params.atividadeid, usuario.id, usuario.tipo);

    return res.status(200).send(result);

  } catch (error) {
    console.log(error);

    return res.status(500).send({ message: 'Internal server error!!' });
  }

}

const criaAtividade = async (req, res, next) => {

  try {

    console.log('#########################');

    console.log(req.body);

    // body
    const { name } = req.body;

    var resultNome = await atividadesServices.validaSeNomeJaExiste(name);
    if (resultNome) {
      return res.status(400).send({
        mensagem: 'Já existe uma atividade com o nome informado.'
      });
    }
    await atividadesServices.criaAtividade(req.body);

    return res.status(200).send({
      mensagem: 'Atividade criada com sucesso'
    });

  } catch (error) {

    console.log(error);

    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

const atualizaAtividade = async (req, res, next) => {

  try {

    const { atividadeid } = req.params;

    await atividadesServices.alteraAtividade(atividadeid, req.body);

    return res.status(200).send({
      mensagem: 'Atividade alterada com sucesso'
    });

  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

const deletarAtividade = async (req, res, next) => {

  try {

    const { params } = req;
    const validationResult = await atividadesServices.validaSeAtividadeExiste(params.atividadeid)

    if (!validationResult)
      return res.status(422).send({
        mensage: 'atividade informada não existe',
      });


    await atividadesServices.deletaAtividade(params.atividadeid);

    return res.status(200).send({
      mensage: 'operação realizada com sucesso',
    });

  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

const realizaInscricao = async (req, res, next) => {

  try {

    const { params, usuario } = req;

    const validationResult = await atividadesServices.validaSeAtividadeExiste(params.atividadeid);

    if (!validationResult)
      return res.status(422).send({
        detalhes: ['atividade informada não existe'],
      });


    const validationSubscription = await inscricoesServices.validaSeUsuarioJaInscrito(params.atividadeid, usuario.id);

    if (validationSubscription)
      return res.status(422).send({
        detalhes: ['usuário ja inscrito'],
      });

    await atividadesServices.addInscricaoAluno(params.atividadeid, usuario.id);

    return res.status(200).send({
      mensage: 'operação realizada com sucesso',
    });

  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });
  }

}
const removeInscricao = async (req, res, next) => {


  try {

    const { params, usuario } = req;


    const atividadeValidation = await atividadesServices.validaSeAtividadeExiste(params.atividadeid);

    if (!atividadeValidation)
      return res.status(422).send({
        detalhes: ['atividade informada não existe'],
      });


    const inscricaoValidation = await inscricoesServices.validaSeInscricaoExiste(params.inscricaoid);

    if (!inscricaoValidation)
      return res.status(422).send({
        detalhes: ['inscrição informada não existe'],
      });


    console.log('params.inscricaoid: ', params.inscricaoid);

    const inscricaoUserValidation = await inscricoesServices.validaSeInscricaoPertenceAoUsuario(params.inscricaoid, usuario.id);
    if (!inscricaoUserValidation)
      return res.status(422).send({
        detalhes: ['operação nao pode ser realizada'],
      });

    await inscricoesServices.removerInscricao(params.inscricaoid)

    return res.status(200).send({
      mensagem: 'operação realizada com sucesso.'
    })

  } catch (error) {

    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });

  }
}

module.exports = {
  atualizaAtividade,
  criaAtividade,
  listarAtividades,
  deletarAtividade,
  listarAtividadePorId,
  realizaInscricao,
  removeInscricao,
  getAtividades,
  getAtividadePorId,
  postAtividadeInscricao,
  deleteAtividadeInscricao,
}